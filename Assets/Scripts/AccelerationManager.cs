﻿using UnityEngine;

public static class AccelerationManager
{
    public static Vector2 GetAccel()
    {
        return Input.acceleration;
    }
}
