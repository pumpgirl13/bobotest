﻿using UnityEngine;

public class Sphere : MonoBehaviour
{
    private Rigidbody _rigidBodyComponent;

    private void Awake()
    {
        _rigidBodyComponent = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Vector3 tilt = AccelerationManager.GetAccel();
        tilt = Quaternion.Euler(90, 0, 0) * tilt;
        tilt.y = 0;
        _rigidBodyComponent.AddForce(tilt, ForceMode.Impulse);  
    }
}
